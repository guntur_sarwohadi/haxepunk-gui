package demo;
import com.haxepunk.graphics.Stamp;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.FormatAlign;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.TextInput;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import flash.geom.Point;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;
import openfl.Assets;
import openfl.display.BitmapData;


/**
 * ...
 * @author Lythom
 */

class LabelDemoPanel extends DemoPanel
{
	
	private var labels:Array<Label>;
	private var bgs:Array<Control> = new Array<Control>();
	
	var ti:TextInput;

	public function new(width:Int, height:Int)
	{
		super(0, 0, width, height, true);
		
		var cursor:Point = new Point(10, 10);
		var margin:Int = 4;
		
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		var l:Label = new Label("Labels and TextInputs", cursor.x, cursor.y, Math.round(width - cursor.x));
		l.align = TextFormatAlign.CENTER;
		l.size = 16;
		l.color = 0x000000;
		addControl(l);
		
		cursor.y += margin + l.height;
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		
		var label:Label = new Label("Testing a simple top left aligned single line label", cursor.x, cursor.y, 300, 40, TextFormatAlign.LEFT, VerticalAlign.TOP);
		Label.addGlobalSyle("red", new TextFormat(null, null, 0xB02424));
		var bgControl = new Control(label.x - 1, label.y - 1);
		bgControl.graphic = new Stamp(new BitmapData(302, 42, false, 0xFFFFFF));
		addControl(bgControl); 
		bgs.push(bgControl);
		addControl(label);
		
		cursor.y += margin + label.height + margin;
		label = new Label("Testing a simple <red>bottom center</red> aligned single line label", cursor.x, cursor.y, 300, 40, TextFormatAlign.CENTER, VerticalAlign.CENTER);
		label.useXmlFormat = true;
		var bgControl = new Control(label.x - 1, label.y - 1);
		bgControl.graphic = new Stamp(new BitmapData(302, 42, false, 0xD3D3D3));
		addControl(bgControl);
		bgs.push(bgControl);
		addControl(label);
		
		cursor.y += margin + label.height + margin;
		label = new Label("Testing a simple <big>middle right</big> aligned single line label", cursor.x, cursor.y, 300, 40, TextFormatAlign.RIGHT, VerticalAlign.BOTTOM);
		label.addStyle("big", new TextFormat(null, 16));
		label.useXmlFormat = true;
		var bgControl = new Control(label.x - 1, label.y - 1);
		bgControl.graphic = new Stamp(new BitmapData(302, 42, false, 0xC5C5C5));
		addControl(bgControl);
		bgs.push(bgControl);
		addControl(label);
		
		cursor.y += margin + label.height;
		label = new Label("Testing a simple <purple>left</purple> aligned \nforced two line label", cursor.x, cursor.y, 300, 0);
		label.addStyle("purple", new TextFormat(null, null, 0x9D3789));
		label.useXmlFormat = true;
		addControl(label);
		
		cursor.y += margin + label.height;
		label = new Label("Testing a simple center aligned \nforced two line label", cursor.x, cursor.y, 300, 0,TextFormatAlign.CENTER);
		addControl(label);
		
		cursor.y += margin + label.height;
		label = new Label("Testing a simple right aligned \nforced two line label", cursor.x, cursor.y, 300, 0,TextFormatAlign.RIGHT);
		addControl(label);
		
		cursor.y += margin + label.height;
		label = new Label("Testing a multiline autowrap left aligned label with a lot of text to see how the text does actually react in this component.", cursor.x, cursor.y, 330, 39);
		label.multiline = true;
		addControl(label);
		
		cursor.y += margin + label.height;
		label = new Label("Testing a multiline autowrap center aligned label with a lot of text to see how the text does actually react in this component.", cursor.x, cursor.y, 330, 39,TextFormatAlign.CENTER);
		label.multiline = true;
		addControl(label);
		
		cursor.y += margin + label.height;
		label = new Label("Testing a multiline autowrap right aligned label with a lot of text to see how the text does actually react in this component.", cursor.x, cursor.y, 330, 39,TextFormatAlign.RIGHT);
		label.multiline = true;
		addControl(label);
		
		cursor.y += margin + label.height;
		ti = new TextInput("Testing text input", cursor.x, cursor.y, 300, 40, true, false);
		addControl(ti);
		
		cursor.y = globalInstructionLabel.y + globalInstructionLabel.height + margin;
		cursor.x = globalInstructionLabel.x;
		var instructions:String = "Specific Instructions :\n";
		instructions += "+ - increase text size\n";
		instructions += "- - decrease text size\n";
		instructions += "UP - scroll labels\n";
		instructions += "DOWN -scroll labels\n";
		instructions += "f - switch font family\n";
		l = new Label(instructions, cursor.x, cursor.y);
		addControl(l);
		
	}
	
	override public function addControl(child:Control, ?position:Int):Void
	{
		if (labels == null) {
			labels = new Array<Label>();
		}
		if (Std.is(child, Label)) {
			labels.push(cast(child,Label));
		}
		super.addControl(child, position);
	}
	
	var i = 0;
	
	override public function update()
	{
		if (!enabled || ti.hasFocus) {
			return;
		}
		if (Input.pressed(Key.UP)) {
			for (b in labels) {
				b.localY += 10;
			}
			for (b in bgs) {
				b.localY += 10;
			}
			
		}
		if (Input.pressed(Key.DOWN)) {
			for (b in labels) {
				b.localY -= 10;
			}
			for (b in bgs) {
				b.localY -= 10;
			}
		}
		if (Input.pressed(Key.NUMPAD_ADD)) {
			for (b in labels) {
				b.size++;
			}
		}
		if (Input.pressed(Key.NUMPAD_SUBTRACT)) {
			for (b in labels) {
				b.size--;
			}
		}
		if (Input.pressed(Key.F)) {
			for (b in labels) {
				if (b.font == Assets.getFont("font/pf_ronda_seven.ttf").fontName) {
					b.font = Assets.getFont("font/04B_03__.ttf").fontName;
				} else {
					b.font = Assets.getFont("font/pf_ronda_seven.ttf").fontName;
				}
			}
		}
		super.update();
	}
	
}